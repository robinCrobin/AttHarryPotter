//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Personagens: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var lista_personagens:[Personagens] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lista_personagens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let personagem = self.lista_personagens[indexPath.row]
        print(personagem)
        
        cell.name.text = personagem.name
        cell.actor.text = personagem.actor
        cell.img.kf.setImage(with: URL(string: personagem.image))

        
        return cell
    }

    @IBOutlet weak var tableView_personagens: UITableView!
    
    func getNovoPersonagem() {
            AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Personagens].self) { response in
                if let personagem = response.value {
                    self.lista_personagens = personagem
                }
                self.tableView_personagens.reloadData()
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView_personagens.dataSource = self
        self.tableView_personagens.delegate = self
        
        getNovoPersonagem()
    }

}

